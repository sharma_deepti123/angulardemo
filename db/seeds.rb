# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
city = City.find_by(name: 'Delhi')
city.locations.create(name: 'red fort')
city.locations.create(name: 'lotus temple')
city.locations.create(name: 'India gate')
city = City.find_by(name: 'Mumbai')
city.locations.create(name: 'gateway of India')
city.locations.create(name: 'essel world')
city.locations.create(name: 'aksa beach')
city = City.find_by(name: 'Kolkata')
city.locations.create(name: 'victoria memorial')
city.locations.create(name: 'nicco park')
city.locations.create(name: 'sundarbans')
city = City.find_by(name: 'chennai')
city.locations.create(name: 'marina beach')
city.locations.create(name: 'covelong')
city.locations.create(name: 'kishkinta')
city = City.find_by(name: 'Bangalore')
city.locations.create(name: 'ISCKON temple')
city.locations.create(name: 'cubbon park')
city.locations.create(name: 'bugle rock')
class Place
  include Mongoid::Document
  belongs_to :location
  belongs_to :city
  field :user_id, type: String
  field :time, type: String
end

class City
  include Mongoid::Document
  field :name, type: String
  has_many :locations
end

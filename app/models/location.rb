class Location
  include Mongoid::Document
   field :name, type: String
  belongs_to :city
end

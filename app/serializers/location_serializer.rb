class LocationSerializer < ActiveModel::Serializer
  attributes :name, :id
end

class PlaceSerializer < ActiveModel::Serializer
  attributes :time, :id 
  has_one :city, serializer: CitySerializer
  has_one :location, serializer: LocationSerializer
end

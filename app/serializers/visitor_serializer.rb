class VisitorSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :reason, :phone_number, :address
end

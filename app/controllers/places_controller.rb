class PlacesController < ApplicationController
	respond_to :json
  	def index
	  	cities = City.all
	   respond_to do |format|
	      format.json { render :json => ActiveModel::ArraySerializer.new(cities, each_serializer: CitySerializer) }
	      format.html
	    end
  	end

  	def locations
  		locations =  City.find(params[:id]).locations
  		render :json =>  ActiveModel::ArraySerializer.new(locations, each_serializer: LocationSerializer)
	end

	def places
		places = Place.where(user_id: params[:user_id])
		render :json => ActiveModel::ArraySerializer.new(places, each_serializer: PlaceSerializer) 
	end

	def get_place
		place = Place.find(params[:place_id])
		render :json => PlaceSerializer.new(place, root: false)
	end

	def create
		place = Place.create(user_id: params[:user_id], city_id: params[:city]["id"]["$oid"], location_id: params[:location]["id"]["$oid"], time: params[:time])
		render :json => PlaceSerializer.new(place, root: false)
	end

	def destroy
		place = Place.find(params[:id])
		respond_with place.destroy
	end

	def update
		place = Place.find(params[:place_id])
		place.update_attributes(city_id: params[:city]["id"]["$oid"], location_id: params[:location]["id"]["$oid"], time: params[:place][:time])
		respond_with place
	end

end

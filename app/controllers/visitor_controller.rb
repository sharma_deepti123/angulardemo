class VisitorController < ApplicationController
	before_filter :visitor_params, only: [:create, :update]
  	respond_to :json
	  def index
	  	visitors = Visitor.all
	    respond_to do |format|
	      format.json { render :json => ActiveModel::ArraySerializer.new(visitors, each_serializer: VisitorSerializer) }
	      format.html
	    end
	  end

	  def create
	    respond_with Visitor.create(visitor_params)
	  end

	  def destroy
	    respond_with Visitor.find(params[:id]).destroy
	  end

	  def findVisitor
	  	respond_with Visitor.find(params[:id])
	  end

	  def update
	  	visitor = Visitor.find(params[:id])
	  	visitor.update_attributes(visitor_params)
	  	respond_with visitor
	  end

	private
	  def visitor_params
	    params.permit(:first_name, :last_name, :reason, :phone_number, :address)
	  end

end

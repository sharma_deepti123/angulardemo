angular.module('Demo').directive('nameInput', function() {
    return {
      restrict: 'A',
      require: 'ngModel',
     	link: function(scope, element, attr, ctrl) {
		    function customValidator(ngModelValue) {
		        if (/[0-9]/.test(ngModelValue)) {
		            ctrl.$setValidity('nameValidator', false);
		        } else {
		            ctrl.$setValidity('nameValidator', true);
		        }
		        return ngModelValue;
		    }
		    ctrl.$parsers.push(customValidator);
		}
    };
});
angular.module('Demo').directive('numberInput', function() {
    return {
      require: 'ngModel',
     	link: function(scope, element, attr, ctrl) {
		    function numbersValidator(ngModelValue) {
		        if (ngModelValue.toString().length > 10 || ngModelValue.toString().length <10) {
		            ctrl.$setValidity('numberValidator', false);
		        } else {
		            ctrl.$setValidity('numberValidator', true);
		        }
		        return ngModelValue;
		    }
		    ctrl.$parsers.push(numbersValidator);
		}
    };
});
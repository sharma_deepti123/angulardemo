angular.module('Demo').controller("visitorsController",['auth', 'store','$scope', 'Visitor', '$routeParams', '$location', '$state', function(auth, store,$scope, Visitor, $routeParams, $location, $state) {

  if($routeParams != undefined && $routeParams.id != undefined)
  {
    $scope.editVisitor = Visitor.findVisitor({'id': $routeParams.id},function(data) {
        $scope.editVisitor = data.visitor
    }, function(error) {
        console.log(error);
    });
  }

  $scope.visitors = Visitor.query();
  $scope.addVisitor = function() {
    if ($scope.visitors_form.$valid) {
        $scope.$emit('addVisitor')
        visitor = Visitor.create($scope.newVisitor);
        $scope.visitors.push(visitor)
        $scope.newVisitor = {}
      }
      else
      {
        return false;
      }
   
  }

    $scope.$on('parestController',function(event,data){
      console.log('you are in parent cotroller');
    });

  $scope.deleteVisitor = function(index) {
    
    visitor = $scope.visitors[index];
    Visitor.delete({},{'id': visitor.id.$oid});
    $scope.visitors.splice(index, 1);
  }

  $scope.editVisitor = function(){
    visitor = $scope.visitors[index];
    $state.go('edit_visitor', {'id': visitor.id.$oid})
  }

  $scope.updateVisitor = function() {
    console.log($scope.editVisitor.id.$oid)
    visitor = Visitor.update($scope.editVisitor, {'id': $scope.editVisitor.id.$oid});
    $state.go('index');
  }
 
    $scope.logout = function() {
    auth.signout();
    store.remove('profile');
    store.remove('token');
    $scope.user = false;
    $state.go('login');
  }

  $scope.places = function(){
   var id = store.get('profile').clientID;
   $state.go('places', {'id': id});
  }

}]);
angular.module('Demo').controller('PlaceCtrl', ['$rootScope','$scope', 'Restangular', '$routeParams','Flash','$stateParams','$state',
	function($rootScope, $scope, Restangular,$routeParams, Flash,$stateParams, $state) {
	var service = Restangular.service('places');

	if($stateParams != undefined)
	{
		if ($stateParams.id != undefined)
    		var user_id = $stateParams.id
    	if ($stateParams.place_id != undefined)
    	{
    		Restangular.all('places').customGET('get_place', {'place_id': $stateParams.place_id}).then(function(resp){
				$scope.editPlace = resp;
			});;
    	}
    }

	service.getList().then(function(resp){
		$scope.cities = resp;
	});

	
	$scope.getLoctaions = function(){
		if ($scope.places != undefined)
			var city = Restangular.one('places', $scope.places.city.id.$oid);
		else
			var city = Restangular.one('places', $scope.editPlace.city.id.$oid);
		city.getList('locations', {'user_id': user_id}).then(function(resp){
			$scope.locations =  resp;
		});
	}

	$scope.addPlace = function() {
     	service.post($scope.places, {'user_id': user_id}).then(function(resp){
	        $scope.user_places.push(resp)
        });
        $scope.places = {}
        var message = '<strong> Well done!</strong>  You successfully read this important alert message.';
    	Flash.create('success', message);
  	}


	$scope.get_places = function(){
		Restangular.all('places').customGET('user_places', {'user_id': user_id}).then(function(resp){
			$scope.user_places = resp
		});;
	}

	$scope.deletePlace = function(index){
		var place = Restangular.one('places', $scope.user_places[index].id.$oid);
	    place.remove({'id': $scope.user_places[index].id.$oid});
	    $scope.user_places.splice(index, 1);
	}

	$scope.updatePlace = function() {
	   	$scope.editPlace.put({'place_id': $scope.editPlace.id.$oid});
	    $state.go('places', {'id': user_id});
	  }

}]);
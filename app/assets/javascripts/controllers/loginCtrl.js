angular.module('Demo').controller('LoginCtrl', ['$state','$scope', 'auth', 'store', '$location',
function ($state,$scope, auth, store, $location) {

  if (store.get('token'))
  {
    $scope.user = true;
    $state.go('index');
  }
  else
  {
    $state.go('login');
  }

  $scope.$on('addVisitor', function(event, data){
    $scope.$broadcast('parestController');
  });
  $scope.login = function () {
    auth.signin({}, function (profile, token) {
      store.set('profile', profile);
      store.set('token', token);
      $scope.user = true;
      $state.go('index');
    }, function () {
      // Error callback
    });
  }


}]);
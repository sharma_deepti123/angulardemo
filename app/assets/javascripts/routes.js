angular.module('Demo').config(function($stateProvider, $urlRouterProvider) {
    
    $stateProvider
        
        .state('edit_place', {
            url: '/edit/{place_id}',
            templateUrl: 'templates/edit_place.html',
            controller: 'PlaceCtrl'
        })
        .state('edit_visitor', {
          url: '/edit/:id',
          templateUrl: '/templates/edit.html',
          controller: 'visitorsController'
        })
        .state('index', {
          url: '/index',
          templateUrl: '/templates/index.html',
          controller: 'visitorsController'
        })
        .state('login', {
          url: '/login',
          templateUrl: '/assets/login/login.html',
          controller: 'LoginCtrl'
        })
        .state('places', {
          url: '/places/:id',
          templateUrl: '/templates/places.html',
          controller: 'PlaceCtrl'
        })
              
        
});
// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require angular
//= require angular-route
//= require angular-rails-templates
//= require auth0-angular
//= require a0-angular-storage
//= require angular-jwt
//= require auth0-lock
//= require angular-resource
//= require angular-cookies 
//= require angular-bootstrap
//= require angular-ui-router
//= require angular-route
//= require angular-messages
//= require restangular
//= require lodash
//= require angular-flash
//= require_tree .

angular.module('Demo').factory("Visitor", function($resource, $routeParams) {
  return $resource("visitor/:id", { id: '@id' }, {
    findVisitor:  { method: 'POST',responseType: 'json',isArray: false},
  	query:  { method: 'GET', responseType: 'json', isArray: true },
    index:  {method: 'GET',transformResponse: function(data) {
        return angular.fromJson(data).visitors;
      },
      isArray: true
    },
    update:  { method: 'PUT', responseType: 'json' },
    create:  { method: 'POST', responseType: 'json' }
  });
})

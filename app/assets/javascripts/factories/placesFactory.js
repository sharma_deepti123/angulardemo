angular.module('Demo').factory("Place", function(Restangular) {
  return Restangular.service("places");
})
